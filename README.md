# Autenticação por certificado

Este é exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia Java. 

Este exemplo apresenta os passos necessários para a geração de assinatura XML utilizando-se chave privada armazenada em disco.
  - Passo 1 (Cliente): Carregamento da chave privada e conteúdo do certificado digital.
  - Passo 2 (Servidor): Geração de um desafio.
  - Passo 3 (Cliente): Assinatura do desafio.
  - Passo 4 (Servidor): Verificação da assinatura do desafio.

### Tech

O exemplo utiliza das bibliotecas Java abaixo:
* [BouncyCastle] - A Java Crypto API!
* [Spring] - Used to send HTTP Request
* [Gson] - Used to handle JSONs
* [JDK 8] - Java 8

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastra-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatório a posse de um certificado digital que identifica o autor do artefato assinado que será produzido.

Por esse motivo, é necessário configurar a localização deste arquivo no computador, bem como a senha para acessar seu conteúdo.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| URL_SERVER | URL do Hub-Signer | ServiceConfig
| ACCESS_TOKEN | Access Token para o consumo do serviço (JWT). | ServiceConfig
| PRIVATE_KEY_LOCATION | Localização do arquivo do keystore com a chave privada a ser configurada na aplicação. | CertificateConfig
| PRIVATE_KEY_PASSWORD | Senha do keystore com chave privada a ser configurada na aplicação. | CertificateConfig
| HASH_ALGORITHM | Algoritmo utilizado na assinatura do desafio. | ChallengeConfig
| MODE | Modo de geração do relatório do desafio. 'BASIC' retorna apenas o status do certificado final e 'CHAIN' de toda a cadeia. | ChallengeConfig

## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  

### Uso

Para execução da aplicação de exemplo, compile diretamente as classes ou importe o projeto em sua IDE de preferência.
Utilizamos o JRE versão 8 para desenvolvimento e execução.



   [RestAssured]: <http://rest-assured.io/>
   [BouncyCastle]: <https://www.bouncycastle.org/>
   [FasterXML]: <http://fasterxml.com>
   [JDK 8]: <https://www.oracle.com/java/technologies/javase-jdk8-downloads.html>

