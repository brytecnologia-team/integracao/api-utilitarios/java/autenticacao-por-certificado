package br.com.bry.exemplos.utilitarios;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Base64;

import org.springframework.web.client.RestClientException;

import br.com.bry.exemplos.utilitarios.config.CertificateConfig;
import br.com.bry.exemplos.utilitarios.config.ChallengeConfig;
import br.com.bry.exemplos.utilitarios.crypto.Signer;
import br.com.bry.exemplos.utilitarios.dto.RequisicaoAutenticacao;
import br.com.bry.exemplos.utilitarios.dto.ResultadoAutenticacao;
import br.com.bry.exemplos.utilitarios.enums.HashAlgorithm;
import br.com.bry.exemplos.utilitarios.exception.HubException;
import br.com.bry.exemplos.utilitarios.servidorautenticacao.ServidorAutenticacao;

public class ExemploAutenticacaoPorCertificado {

	public static void main(String[] args) {
		try {
			System.out.println("========== Iniciando processo de autenticação ==========");
			
			/* Lê a chave privada e o certificado do key store */
			Signer signer = new Signer(CertificateConfig.PRIVATE_KEY_LOCATION, CertificateConfig.PRIVATE_KEY_PASSWORD);
			
			/* Gera um desafio de autenticação. Em um ambiente real, o desafio deve ser gerado pelo servidor, não pelo usuário */
			byte[] desafio = ServidorAutenticacao.gerarDesafioAutenticacao();
			
			/* Assina o desafio */
			byte[] assinaturaDesafio = signer.sign(HashAlgorithm.valueOf(ChallengeConfig.HASH_ALGORITHM), desafio);
			
			RequisicaoAutenticacao requisicaoAutenticacao = new RequisicaoAutenticacao();
			requisicaoAutenticacao.setCertificate(Base64.getEncoder().encodeToString(signer.getCertificate()));
			requisicaoAutenticacao.setMode(ChallengeConfig.MODE);
			requisicaoAutenticacao.setHashAlgorithm(ChallengeConfig.HASH_ALGORITHM);
			requisicaoAutenticacao.setDesafio(Base64.getEncoder().encodeToString(desafio));
			requisicaoAutenticacao.setAssinaturaDesafio(Base64.getEncoder().encodeToString(assinaturaDesafio));
			
			/* Envia a resposta do desafio para o servidor, que o encaminha para ser verificado no Hub-Signer. Recebe a resposta */
			ResultadoAutenticacao resultadoAutenticacao = ServidorAutenticacao.autenticar(requisicaoAutenticacao);
			
			System.out.printf("Status geral do certificado: %s%n", resultadoAutenticacao.getStatusCertificado());
			System.out.printf("Assinatura verificada: %b%n", resultadoAutenticacao.isAssinaturaVerificada());

			/* Se tanto o certificado e a assinatura foram válidos, o usuário está autenticado, caso contrário, a autenticação falhou */
			if (resultadoAutenticacao.isAssinaturaVerificada() && resultadoAutenticacao.getStatusCertificado().equals("VALID")) {
				System.out.println("Usuário autenticado com sucesso");
			} else {
				System.out.println("Falha ao autenticar usuário");
			}
		} catch (IOException e) {
			System.err.print("Falha ao abrir o Key Store, verifique a senha e o caminho.");
		} catch (URISyntaxException e) {
			System.err.println("URL do Hub-Signer mal-formada");
		} catch (HubException e) {
			System.err.println(e.getErrorResponseAsJSON());
		}
		System.out.println("========== Processo de autenticação finalizado ==========");
	}

}
