package br.com.bry.exemplos.utilitarios.config;

public class CertificateConfig {

	private CertificateConfig() {}
	
	// If you don't have one, follow the instructions in README.md on how to obtain your personal private key.
	public static final String PRIVATE_KEY_LOCATION = "<PRIVATE_KEY_LOCATION>";
	
	public static final String PRIVATE_KEY_PASSWORD = "<PRIVATE_KEY_PASSWORD>";
}
