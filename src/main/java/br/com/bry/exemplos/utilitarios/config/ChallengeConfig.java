package br.com.bry.exemplos.utilitarios.config;

public class ChallengeConfig {
	
	private ChallengeConfig() {}

	// Valores disponíveis: "SHA1", "SHA256" e "SHA512".
	public static final String HASH_ALGORITHM = "SHA256";
	
	// Valores disponíveis: "BASIC" e "CHAIN".
	public static final String MODE = "BASIC";
	
}
