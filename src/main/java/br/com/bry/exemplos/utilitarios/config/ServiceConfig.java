package br.com.bry.exemplos.utilitarios.config;

public class ServiceConfig {
	
	private ServiceConfig() {}

	public static final String URL_SERVER = "https://hub2.bry.com.br/api/authentication-service/v1/verify-challenge";
	
	/* URL do ambiente de homologação */
    //	public static final String URL_SERVER = "https://hub2.hom.bry.com.br/api/authentication-service/v1/verify-challenge";
	
	public static final String ACCESS_TOKEN = "<ACCESS_TOKEN>";
}
