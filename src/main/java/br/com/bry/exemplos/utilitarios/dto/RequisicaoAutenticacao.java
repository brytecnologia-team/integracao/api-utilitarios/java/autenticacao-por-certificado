package br.com.bry.exemplos.utilitarios.dto;

public class RequisicaoAutenticacao {

	private String certificate;
	private String mode;
	private String hashAlgorithm;
	private String desafio;
	private String assinaturaDesafio;
	
	public String getCertificate() {
		return certificate;
	}
	
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	
	public String getMode() {
		return mode;
	}
	
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	public String getHashAlgorithm() {
		return hashAlgorithm;
	}
	
	public void setHashAlgorithm(String hashAlgorithm) {
		this.hashAlgorithm = hashAlgorithm;
	}
	
	public String getDesafio() {
		return desafio;
	}
	
	public void setDesafio(String desafio) {
		this.desafio = desafio;
	}
	
	public String getAssinaturaDesafio() {
		return assinaturaDesafio;
	}
	
	public void setAssinaturaDesafio(String assinaturaDesafio) {
		this.assinaturaDesafio = assinaturaDesafio;
	}
	
}
