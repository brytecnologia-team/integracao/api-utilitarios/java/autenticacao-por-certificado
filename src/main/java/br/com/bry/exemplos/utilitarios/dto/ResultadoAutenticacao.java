package br.com.bry.exemplos.utilitarios.dto;

public class ResultadoAutenticacao {

	private String statusCertificado;
	private boolean assinaturaVerificada;
	
	public String getStatusCertificado() {
		return statusCertificado;
	}
	
	public void setStatusCertificado(String statusCertificado) {
		this.statusCertificado = statusCertificado;
	}
	
	public boolean isAssinaturaVerificada() {
		return assinaturaVerificada;
	}
	
	public void setAssinaturaVerificada(boolean assinaturaVerificada) {
		this.assinaturaVerificada = assinaturaVerificada;
	}
	
}
