package br.com.bry.exemplos.utilitarios.enums;

public enum HashAlgorithm {
	SHA1, SHA256, SHA512
}
