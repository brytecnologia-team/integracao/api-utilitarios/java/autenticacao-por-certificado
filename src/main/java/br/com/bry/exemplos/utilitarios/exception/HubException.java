package br.com.bry.exemplos.utilitarios.exception;

public class HubException extends Exception {

	private static final long serialVersionUID = 160372226582424410L;

	private final String errorResponseAsJSON;
	
	public HubException(String errorResponseAsJSON) {
		super("Falha na comunicação com o Hub Signer. Requisição mal-formada.");
		this.errorResponseAsJSON = errorResponseAsJSON;
	}
	
	public String getErrorResponseAsJSON() {
		return this.errorResponseAsJSON;
	}
	
}
