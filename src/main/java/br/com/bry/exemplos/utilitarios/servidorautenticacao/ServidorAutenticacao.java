package br.com.bry.exemplos.utilitarios.servidorautenticacao;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Random;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import br.com.bry.exemplos.utilitarios.config.ServiceConfig;
import br.com.bry.exemplos.utilitarios.dto.RequisicaoAutenticacao;
import br.com.bry.exemplos.utilitarios.dto.ResultadoAutenticacao;
import br.com.bry.exemplos.utilitarios.exception.HubException;

/**
 * Classe representando o servidor de autenticação. Em um ambiente real de produção,
 * estas etapas devem ser realizadas pelo servidor que deseja autenticar os usuários
 * e não pelo próprio usuário.
 */
public class ServidorAutenticacao {
		
	private static final Random RANDOM = new Random();
	
	private ServidorAutenticacao() {}

	public static byte[] gerarDesafioAutenticacao() {
		byte[] desafio = new byte[128];
		RANDOM.nextBytes(desafio);
		return desafio;
	}
	
	public static ResultadoAutenticacao autenticar(RequisicaoAutenticacao requisicao) throws URISyntaxException, HubException {
		
		System.out.printf("Certificado: %s%n", requisicao.getCertificate());
		System.out.printf("Modo: %s%n", requisicao.getMode());
		System.out.printf("Algoritmo de hash: %s%n", requisicao.getHashAlgorithm());
		System.out.printf("Desafio: %s%n", requisicao.getDesafio());
		System.out.printf("Assinatura do desafio: %s%n", requisicao.getAssinaturaDesafio());

		RestTemplate restTemplate = new RestTemplate();

		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", "Bearer " + ServiceConfig.ACCESS_TOKEN);
		headers.add("Content-Type", "application/json");

		JsonObject requestAsJSON = new JsonObject();
		requestAsJSON.addProperty("certificate", requisicao.getCertificate());
		requestAsJSON.addProperty("mode", requisicao.getMode());
		requestAsJSON.addProperty("hashAlgorithm", requisicao.getHashAlgorithm());
		requestAsJSON.addProperty("challenge", requisicao.getDesafio());
		requestAsJSON.addProperty("challengeSignature", requisicao.getAssinaturaDesafio());
		
		HttpEntity<String> requestEntity = new HttpEntity<String>(requestAsJSON.toString(), headers);
		
		ResponseEntity<String> responseEntity;
		try {
			responseEntity = restTemplate.postForEntity(new URI(ServiceConfig.URL_SERVER), requestEntity, String.class);			
		} catch (RestClientException e) {
			throw new HubException(e.getMessage());
		}
		
		if(responseEntity.getStatusCodeValue() == 200) {
			JsonObject responseBody = JsonParser.parseString(responseEntity.getBody()).getAsJsonObject();
			/* Lê o status da assinatura do JSON de resposta */
			String signatureVerified = responseBody.get("signatureVerified").getAsString();

			/* 
			 * Lê o status do certificado do JSON de resposta. Aqui está sendo lido apenas o status geral,
			 * mas pode-se ler outras informações mais detalhadas sobre a verificação.
			 */
			String certificateStatus = responseBody.get("certificateReport").getAsJsonObject().get("status").getAsJsonObject().get("status").getAsString();
			
			ResultadoAutenticacao resultadoAutenticacao = new ResultadoAutenticacao();
			resultadoAutenticacao.setAssinaturaVerificada(Boolean.valueOf(signatureVerified));
			resultadoAutenticacao.setStatusCertificado(certificateStatus);
			
			return resultadoAutenticacao;
		} else {
			throw new RuntimeException("Falha ao se comunicar com o Hub-Signer.");
		}

	}
	
}
